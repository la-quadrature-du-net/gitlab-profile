# Les projets de la Quadrature du Net

Ce groupe rassemble les projets libres de LQDN. Il y a autant des projets qui servent la Quadrature du Net en interne, que des projets portés par la Quadrature, que des projets hébergé par la Quadrature sans que ça soit directement utile, mais que l'on soutient. 

Les contributions sont les bienvenues ! 